import Layout from '../components/Layout';
import Link from 'next/link';

const PostLink = ({title,slug}) => (

    <Link as={`${slug}`} href={`/post?title=${title}`}><a>{title}</a></Link>
)

const Blog = () => (
    <Layout title="My blog">
        <PostLink slug="react" title="React" />
        <PostLink title="Angular" />
        <PostLink title="Vue" />
    </Layout>
    
)

export default Blog;