import Layout from '../components/Layout';

export default ({statusCode}) => (

    <Layout title="Error">

    {statusCode ? `Couldnt find any data: Status Code:${statusCode}  ` : `Couldnt find that page`}
        
    </Layout>
);