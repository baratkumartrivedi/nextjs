import Document, {Head, Main, NextScript} from 'next/document';

export default class MyDocument extends Document {
    return(){
        render(

            <html>
                <Head>
                <title>Page Title</title>
        <link href="https://cdnjs.cloudflare.com/ajax/libs/nprogress/0.2.0/nprogress.min.css" rel="stylesheet" />
                </Head>
                <body>
                    <Main />
                    <NextScript />
                </body>
                <style jsx>{`
                body{
                    background:red;
                }
                
                `}</style>
                
            </html>

        );
    }
}