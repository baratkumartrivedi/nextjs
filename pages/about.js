import Layout from '../components/Layout';
import {Component} from 'react';
import fetch from 'isomorphic-unfetch';
import Error from './_error';

class About extends Component {
    static async getInitialProps()
    {
       const res = await fetch("https://api.github.com/users/simonjefford");
       const data = await res.json();
       const statusCode = res.status > 200 ? res.status : false;
       return {user: data, statusCode};
        
    }
   render(){
       const {user,statusCode} = this.props;
       if(statusCode)
       {
           return <Error statusCode={statusCode} />
       }
       return(
        <Layout title="About">
            <p>{user.login}</p>
            <img src={user.avatar_url} width="200"/>
        </Layout>
       );
   }
}

export default About;