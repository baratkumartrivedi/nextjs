import Link from 'next/link';
import Router from 'next/router';
import Head from 'next/head';
import NProgress from 'nprogress';


Router.onRouteChangeStart = url => {
    console.log(url);
    NProgress.start();

}
Router.onRouteChangeComplete = () => NProgress.done();
const Layout = ({children,title})  => (

<div>
    <Head>
        <title>Hey</title>       
    </Head>
<header>
    <Link href="/"><a>Home</a></Link>
    <Link href="/about"><a>About</a></Link>
</header>

<h1>{title}</h1>
{children}
<footer>&copy; {new Date().getFullYear()}</footer>
</div>
);
export default Layout;